import { Component } from '@angular/core';

import { DUIFormControlDataType, DUIFormControlType, DUIFormControlValidatorType } from 'documentum-ui-lib'
import { DUIForm } from 'projects/documentum-ui-lib/src/lib/models/dui-form-api/dui-form';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'documentum-client-app';

  F: DUIForm = {
    code: 'predmet-create',
    title: "Novi predmet",
    layout: {
      width: 50,
      sections: [
        {
          title: null,
          collapsable: false,
          rows: [
            {
              controls: [
                {
                  attributeName: "brojPredmeta",
                  controlType: DUIFormControlType.TEXT_INPUT,
                  dataType: DUIFormControlDataType.TEXT,
                  disabled: true,
                  hidden: false,
                  label: "Broj predmeta",
                  predefinedValue: null,
                  width: 50,
                  validators: [
                    {
                      validatorType: DUIFormControlValidatorType.REQUIRED,
                      validationValue: null
                    },
                    {
                      validationValue: "[0-9\-]{0, 30}",
                      validatorType: DUIFormControlValidatorType.PATTERN
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    }
  }
}
