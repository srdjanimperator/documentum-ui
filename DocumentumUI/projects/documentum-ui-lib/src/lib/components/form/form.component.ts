import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn } from '@angular/forms';
import { DUIFormControlType } from '../../models/dui-form-api/dui-form-control-type.enum';
import { DUIForm } from '../../models/dui-form-api/dui-form';

@Component({
  selector: 'dui-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit, OnChanges {

  @Input("F") F: DUIForm | null = null;

  fg: FormGroup = this.fb.group({});

  DUIFormControlType = DUIFormControlType;

  constructor(private fb: FormBuilder) { }

  async ngOnChanges(changes: SimpleChanges) { 
    if (changes?.F) {
      this.initializeFormGroup();
    }
  }

  initializeFormGroup() {
    this.F?.layout.sections.forEach(section => {
      section.rows.forEach(row => {
        row.controls.forEach(control => {
          let validators: Array<ValidatorFn> = [];
          this.fg.addControl(
            control.attributeName,
            new FormControl({ value: control.predefinedValue, disabled: false }, validators)
          )
        });
      });
    });
  }

  ngOnInit(): void {}

}
