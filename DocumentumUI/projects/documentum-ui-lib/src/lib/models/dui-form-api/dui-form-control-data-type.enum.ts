export enum DUIFormControlDataType {
    TEXT = "TEXT",
    BOOL = "BOOL",
    INT = "INT",
    DOUBLE = "DOUBLE",
    DATE = "DATE",
    ARRAY = "ARRAY"
}