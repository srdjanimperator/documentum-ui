export enum DUIFormControlValidatorType {
    REQUIRED = "REQUIRED",
    PATTERN = "PATTERN",
    DATERANGE = "DATE_RANGE",
    IN_ARRAY = "IN_ARRAY",
    MAX_STR_LENGTH = "MAX_STR_LENGTH", 
    MIN_STR_LENGTH = "MIN_STR_LENGTH",
    MAX = "MAX",
    MIN = "MIN"
}