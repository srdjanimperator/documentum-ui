
import { DUIFormControlDataType } from './dui-form-control-data-type.enum';
import { DUIFormControlType } from './dui-form-control-type.enum'
import { DUIFormControlValidatorType } from './dui-form-control-validator-type.enum';

export interface DUIForm {
    code: string;
    title: string | null;
    layout: DUIFormLayout;
}

export interface DUIFormLayout {
    width: number | null;
    sections: Array<DUIFormSection>;
}

export interface DUIFormSection {
    title: string | null;
    collapsable: boolean | null;
    rows: Array<DUIFormRow>;
}

export interface DUIFormRow {
    controls: Array<DUIFormControl>;
}

export interface DUIFormControl {
    attributeName: string;
    label: string;
    predefinedValue: any;
    controlType: DUIFormControlType;
    dataType: DUIFormControlDataType;
    disabled: boolean;
    hidden: boolean;
    validators: Array<DUIFormControlValidator>;
    width: number | null;
}

export interface DUIFormControlValidator {
    validatorType: DUIFormControlValidatorType;
    validationValue: string | number | null;
}